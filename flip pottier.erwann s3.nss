/* FLIP CODE - DO NOT EDIT
<?xml version="1.0" encoding="utf-16"?><Script BasedOn="flip pottier.erwann s2"><Code><Pegs><Peg><Slot><IfControl X="Non Numérique" Y="Non Numérique"><Condition><Statement X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.IsDead" StatementType="Condition" /><Slots><Slot Index="0"><ObjectBlock X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.Behaviours.InstanceBehaviour" Identifier="c_uthraki" DisplayName="Varian infected" Nwn2Type="Creature" AreaTag="ar_ruin" ResRef="c_uthraki" IconName="" /></ObjectBlock></Slot></Slots></Statement></Condition><Consequences><Spine><Pegs><Peg><Slot><Statement X="222,5" Y="239,5"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.Delete" StatementType="Action" /><Slots><Slot Index="0"><ObjectBlock X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.Behaviours.InstanceBehaviour" Identifier="c_uthraki" DisplayName="Varian infected" Nwn2Type="Creature" AreaTag="ar_ruin" ResRef="c_uthraki" IconName="" /></ObjectBlock></Slot></Slots></Statement></Slot></Peg><Peg><Slot><Statement X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.DisplayMessage" StatementType="Action" /><Slots><Slot Index="0"><StringBlock X="Non Numérique" Y="Non Numérique" Value="abc" /></Slot></Slots></Statement></Slot></Peg></Pegs></Spine></Consequences></IfControl></Slot></Peg><Peg><Slot /></Peg><Peg><Slot /></Peg></Pegs></Code></Script>
FLIP CODE - DO NOT EDIT */

/* FLIP TARGET ADDRESS - DO NOT EDIT
Module|OnHeartbeat
FLIP TARGET ADDRESS - DO NOT EDIT */

/* NATURAL LANGUAGE
Every six seconds:
If Varian infected is dead, then Varian infected is permanently removed from the game and a message pops up ("abc").
NATURAL LANGUAGE */
#include "ginc_param_const"
#include "ginc_actions"
#include "NW_I0_GENERIC"
#include "flip_functions"
#include "ginc_henchman"

void main()
{
if (GetIsDead(GetObjectByTag("c_uthraki",0))) {
DestroyObject(GetObjectByTag("c_uthraki",0),0.0f,TRUE);
DisplayMessageBox(GetFirstPC(),0,"abc");

}


}