/* FLIP CODE - DO NOT EDIT
<?xml version="1.0" encoding="utf-16"?><Script BasedOn=""><Code><Pegs><Peg><Slot><Statement X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.JumpsTo" StatementType="Action" /><Slots><Slot Index="0"><ObjectBlock X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.Behaviours.PlayerBehaviour" Identifier="" DisplayName="player" /></ObjectBlock></Slot><Slot Index="1"><ObjectBlock X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.Behaviours.InstanceBehaviour" Identifier="Exit_Door" DisplayName="{Exit Door}" Nwn2Type="Door" AreaTag="ar_church" ResRef="plc_dt_estate" IconName="" /></ObjectBlock></Slot></Slots></Statement></Slot></Peg><Peg><Slot /></Peg><Peg><Slot /></Peg></Pegs></Code></Script>
FLIP CODE - DO NOT EDIT */

/* FLIP TARGET ADDRESS - DO NOT EDIT
Door|OnUnlock|ar_city|door_church|-1
FLIP TARGET ADDRESS - DO NOT EDIT */

/* NATURAL LANGUAGE
When {Door Temple} is unlocked:
The player teleports to the location of {Exit Door}.
NATURAL LANGUAGE */
#include "ginc_param_const"
#include "ginc_actions"
#include "NW_I0_GENERIC"
#include "flip_functions"
#include "ginc_henchman"

void main()
{
AssignCommand(GetFirstPC(),ActionJumpToObject(GetObjectByTag("Exit_Door",0),TRUE));

}