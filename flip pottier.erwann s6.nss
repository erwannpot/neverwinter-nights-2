/* FLIP CODE - DO NOT EDIT
<?xml version="1.0" encoding="utf-16"?><Script BasedOn="flip pottier.erwann"><Code><Pegs><Peg><Slot><IfControl X="Non Numérique" Y="Non Numérique"><Condition><Statement X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.OwnsItem" StatementType="Condition" /><Slots><Slot Index="0"><ObjectBlock X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.Behaviours.PlayerBehaviour" Identifier="" DisplayName="player" /></ObjectBlock></Slot><Slot Index="1"><ObjectBlock X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.Behaviours.InstanceBehaviour" Identifier="key_catacomb" DisplayName="Key's Catacombs" Nwn2Type="Item" AreaTag="ar_church" ResRef="key_catacomb" IconName="it_qi_brycetombkey" /></ObjectBlock></Slot></Slots></Statement></Condition><Consequences><Spine><Pegs><Peg><Slot><Statement X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.AreaTransition" StatementType="Action" /><Slots><Slot Index="0"><ObjectBlock X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.Behaviours.AreaBehaviour" Identifier="ar_ruin" DisplayName="ar_ruin" IsExterior="false" /></ObjectBlock></Slot><Slot Index="1"><ObjectBlock X="Non Numérique" Y="Non Numérique"><Behaviour Type="Sussex.Flip.Games.NeverwinterNightsTwo.Behaviours.InstanceBehaviour" Identifier="PLC_DT_IronGat02" DisplayName="Portcullis Double Door" Nwn2Type="Door" AreaTag="ar_ruin" ResRef="plc_dt_irongat02" IconName="" /></ObjectBlock></Slot></Slots></Statement></Slot></Peg></Pegs></Spine></Consequences></IfControl></Slot></Peg><Peg><Slot /></Peg><Peg><Slot /></Peg></Pegs></Code></Script>
FLIP CODE - DO NOT EDIT */

/* FLIP TARGET ADDRESS - DO NOT EDIT
Trigger|OnEnter|ar_church|GenericTrigger|-1
FLIP TARGET ADDRESS - DO NOT EDIT */

/* NATURAL LANGUAGE
When something walks into Transfer Catacombs:
If the player is carrying Key's Catacombs in its inventory, then the player teleports to the location of Portcullis Double Door in ar_ruin.
NATURAL LANGUAGE */
#include "ginc_param_const"
#include "ginc_actions"
#include "NW_I0_GENERIC"
#include "flip_functions"
#include "ginc_henchman"

void main()
{
if ((GetItemPossessor(GetObjectByTag("key_catacomb",0)) == GetFirstPC())) {
JumpPartyToArea(GetFirstPC(),GetObjectInArea("PLC_DT_IronGat02",GetAreaFromTag("ar_ruin")));
	object oPC = (GetPCSpeaker()==OBJECT_INVALID?OBJECT_SELF:GetPCSpeaker());
	AddJournalQuestEntry("1dungeon",11,oPC);
}


}