/*

    Script:			This is the MASTER on heartbeat script.  It is primarily used for the Time Plugin and dungeon instances but
					can be used for other things if we ever come up with something to use for it.
	Version:		1.1
	Plugin Version: 2.0
	Author:			Marshall Vyper
	Parameters:		None
	
	Change Log:		10/08/2012 - 1.0 MV - Initial Release
					05/11/2015 - 1.0 MV - Update for World Plugin heartbeats.
					05/13/2015 - 1.0 MV - Update for Time Plugin heartbeats.
*/

// /////////////////////////////////////////////////////////////////////////////////////////////////////
// MAIN ROUTINE
// /////////////////////////////////////////////////////////////////////////////////////////////////////
void main()
{
	object oModule = GetModule();
	
	// Check for the Time Plugin and fire its script if it is.
	if (GetLocalInt(GetModule(), "LEG_TIME_ACTIVE"))
	{
		ExecuteScriptEnhanced("leg_time_modheartbeat", oModule);		
	}
	
	// Check for the Tele Plugin and fire its script if it is.
	if (GetLocalInt(GetModule(), "LEG_TELE_ACTIVE"))
	{
		ExecuteScriptEnhanced("leg_tele_modheartbeat", oModule);		
	}
	
	// Check for the Tele Plugin and fire its script if it is.
	if (GetLocalInt(GetModule(), "LEG_WORLD_ACTIVE"))
	{
		ExecuteScriptEnhanced("leg_world_modheartbeat", oModule);		
	}	
}